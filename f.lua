local gl = require( 'moongl' )

function Clean( a )
	gl.delete_buffers( a.mesh.vao, a.mesh.vbo, a.mesh.tvbo, a.mesh.ebo )
	a.mesh.texture = nil
	gl.clean_program( a.mesh.shader[1] )
	a = nil


	Textures = nil
	collectgarbage( "collect" )
end

local screenZero = -650
--LineHeight = LineHeight * 0.8
local a = Button:New( Texture( 'assets/uiBase.png' ) )

a.mesh.scale = 0.3
a.mesh.color = { 2.0, 2.0, 2.0, 2.0 }
a:Draw( { math.sin( CurrentTime(1) ),0,0 } )

--InitFont( "assets/HasklugFont.otf", 36 )

-- Text font( fonte para usar )
-- Text scale
-- table to store all the text
-- do not draw if position is outside the screen
-- store each line as an item in the table
-- store the screen coordinates like maxx maxy etc
-- store the scroll

local numLines = 1
local scroll = 40

RenderText( LineHeight, -100, -100, 0.2 )
RenderText( 'Viva Cristo Rei! Salve Maria!', screenZero, numLines*LineHeight+scroll, 0.2 )
numLines = numLines + 1
RenderText( 'Viva Cristo Rei! Salve Maria!', screenZero, numLines*LineHeight+scroll, 0.2 )
numLines = numLines + 1
RenderText( 'Viva Cristo Rei! Salve Maria!', screenZero, numLines*LineHeight+scroll, 0.2 )
numLines = numLines + 1


--RenderText( 'world', 10, -25, 10.9 )


--Clean( a )

a.mesh:Clean()

