-- headers
local gl     = require( "moongl"       )
local glfw   = require( "moonglfw"     )
local glmath = require( "moonglmath"   )
local ai     = require( "moonassimp"   )
local mi     = require( "moonimage"    )
local ft     = require( "moonfreetype" )
local vec2        = glmath.vec2
local vec3        = glmath.vec3
local vec4        = glmath.vec4
local clamp       = glmath.clamp
local look_at     = glmath.look_at
local translate   = glmath.translate
local scale       = glmath.scale
local perspective = glmath.perspective
local sin, cos, rad = math.sin, math.cos, math.rad
-- bread's shader uniforms locations
--Uniform = {}

Log               = {}
CollisionItems    = {}
Player            = {}
Keys              = {}
Objects           = {}
PlayerPosition    = { 0, 0, 0    }
DefaultBox        = { {-1,-1}, { 1,-1 } , { 1,1 } , { -1,1 } } --ll, lr, tr, tl
LightPos          = { 0, 10, 0   }
LightColor        = { 1, 1, 1    }
DefaultColor      = { 1, 1, 1, 1 }
MovementDimension = nil
PlayerOrientation = nil
PreviousButton    = nil
ButtonCounter     = 0
Change            = 0
ShaderType        = 0
Collided          = false
WindowHeight      = 1920
WindowWidth       = 1080
LineHeight = 0

local vao
local tvao
local vbo
local textVbo
local textureVbo
local textTextureVbo
local ebo
local fbo
local rbo
local shader
local tshader
local window
local fontTextureTable = {}
local chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()-_+=[]{}';:.,<>?/|/`~/}0123456789 "
local Textures = {}
local firstMouse = true
local lastX, lastY = 500/2, 500/2
local Testhull

-- reshape function
local function reshape( _, w, h )
	Console( "window shape: " .. w .. " " .. h )
	gl.viewport( 0, 0, w, h )
end
-- function to init everything and return the main window
function Init()
	-- create the main window
	window = glfw.create_window( WindowWidth, WindowHeight, "Moon" )

	-- init opengl
	glfw.make_context_current( window )
	gl.init()

	-- antialiasing
	gl.enable( 'multisample' )
	glfw.window_hint( "samples", 16 )

	-- set the callback keys
	glfw.set_window_size_callback( window, reshape )
	--glfw.set_key_callback( window, TestKeys )

	-- mouse callback
	glfw.set_cursor_pos_callback( window, function( window, xpos, ypos )
		if firstMouse then
			lastX, lastY = xpos, ypos
			firstMouse = false
		end

		local xoffset = xpos  - lastX
		local yoffset = lastY - ypos

		lastX, lastY = xpos, ypos
		--ProcessMouse( xoffset, yoffset )

		Console( "Mouse X: " .. xpos .. " Mouse Y: " .. ypos )
		--Raycasting( xpos, ypos )

		--Camera.front = -glmath.mat3( glmath.rotate_y( rad( ( -xpos/1000 ) ) ) ) * Camera.front -- this works uncomment
		--Camera.right = (Camera.front % Camera.up):normalize() -- this works uncomment
		--if( RayCasting( Raycasting( xpos, ypos ), Testhull) ) then print( "Inside" ) else print( "Outside" ) end
	end)

	-- scroll callbak
	glfw.set_scroll_callback( window, function( window, xoffset, yoffset )
		ProcessScroll( yoffset )
	end)

	-- capture the mouse
	glfw.set_input_mode(window, 'cursor', 'normal')

	-- generate the buffers
	vao            = gl.gen_vertex_arrays()
	tvao           = gl.gen_vertex_arrays()
	vbo            = gl.gen_buffers()
	textVbo        = gl.gen_buffers()
	textureVbo     = gl.gen_buffers()
	textTextureVbo = gl.gen_buffers()

	return window
end
-- function to read vertex and frag and use a program
function Shader( shaderType )
--function Shader( vertex, frag, dim )
	local vfile = io.open( "shaders/v.glsl", "rb" )
	if not vfile then print( "Cannot read shader " .. vertex ) end
	local vs = vfile:read( "*all" )
	if vfile then vfile:close() end

	local ffile = io.open( "shaders/f.glsl", "rb" )
	if not ffile then print( "Cannot read shader " .. frag ) end
	local fs = ffile:read( "*all" )
	if ffile then ffile:close() end

	-- note for geometry and tesselation shaders
	-- alter here to only call gl.create_shader the shader if the file exists
	-- and only if needed call compile

	local vertexShader   = gl.create_shader( "vertex" )
	local fragmentShader = gl.create_shader( "fragment" )
	local program        = gl.create_program()

	gl.shader_source( vertexShader  , vs )
	gl.shader_source( fragmentShader, fs )

	gl.compile_shader( vertexShader   , true )
	gl.compile_shader( fragmentShader , true )

	if not gl.get_shader( vertexShader, 'compile status' ) then
		 error(gl.get_shader_info_log( vertexShader ))
	end

	if not gl.get_shader( fragmentShader, 'compile status' ) then
		 error(gl.get_shader_info_log( fragmentShader ))
	end

	gl.attach_shader( program, vertexShader   )
	gl.attach_shader( program, fragmentShader )

	gl.link_program( program, true )

	local uniforms = {}

	table.insert( uniforms , { "model"      , gl.get_uniform_location( program , "model"      ) , 'float' } )
	table.insert( uniforms , { "view"       , gl.get_uniform_location( program , "view"       ) , 'float' } )
	table.insert( uniforms , { "projection" , gl.get_uniform_location( program , "projection" ) , 'float' } )
	table.insert( uniforms , { "position"   , gl.get_uniform_location( program , "position"   ) , 'float' } )
	table.insert( uniforms , { "lightPos"   , gl.get_uniform_location( program , "lightPos"   ) , 'float' } )
	table.insert( uniforms , { "lightColor" , gl.get_uniform_location( program , "lightColor" ) , 'float' } )
	table.insert( uniforms , { "color"      , gl.get_uniform_location( program , "color"      ) , 'float' } )
	--table.insert( uniforms , { "Time"       , gl.get_uniform_location( program , "Time"       ) , 'float' } )
	table.insert( uniforms , { "shaderType" , gl.get_uniform_location( program , "shaderType" ) , 'float' } )
	table.insert( uniforms , { "Orientation" , gl.get_uniform_location( program , "Orientation" ) , 'float' } )

	gl.use_program( program )

	gl.uniform( uniforms[5][2], uniforms[5][3], gl.flatten( LightPos )     )
	gl.uniform( uniforms[6][2], uniforms[6][3], gl.flatten( LightColor )   )
	gl.uniform( uniforms[7][2], uniforms[7][3], gl.flatten( DefaultColor ) )
	--gl.uniform( uniforms[9][2], uniforms[9][3], gl.flatten( { 1.0 } ) )

	if( shaderType == "cel"     ) then gl.uniform( uniforms[8][2], uniforms[8][3], 1 ) end
	if( shaderType == "diffuse" ) then gl.uniform( uniforms[8][2], uniforms[8][3], 2 ) end
	if( shaderType == "2d"      ) then gl.uniform( uniforms[8][2], uniforms[8][3], 3 ) end
	if( shaderType == "text"    ) then gl.uniform( uniforms[8][2], uniforms[8][3], 4 ) end

	local shader = { program, uniforms }

	return shader
end

-- create an image texture
function Texture( filename )
	local texture = gl.gen_textures()
	gl.bind_texture( '2d', texture )

	mi.flip_vertically_on_load(true)

	gl.texture_parameter( '2d', 'wrap s', 'repeat' )
	gl.texture_parameter( '2d', 'wrap t', 'repeat' )
	gl.texture_parameter( '2d', 'mag filter', 'linear' )
	gl.texture_parameter( '2d', 'min filter', 'linear mipmap linear' )

	local data, w, h, channels = mi.load( filename )
	local intformat, format
	Console( filename .. " channels: " .. channels )
	if channels=='rgba' then
	   intformat = 'srgb alpha' or 'rgba'
	   format = 'rgba'
	elseif channels=='rgb' then
	   intformat = 'rgb'
	   format = 'rgb'
	elseif channels=='y' then intformat, format = 'red', 'red'
	elseif channels=='ya' then intformat, format = 'rg', 'rg' -- is this correct?
	end
	gl.texture_image( '2d', 0, intformat, format, 'ubyte', data, w, h )
	gl.generate_mipmap( '2d' )

	--table.insert( Textures, texture )

	return texture
end

function ProcessInput()
	if MovementDimension == '2d' then
		local chars = { 'a','b','c',
				'd','e','f',
				'g','h','i',
				'j','k','l',
				'm','n','o',
				'p','q','r',
				's','t','u',
				'v','w','x',
				'y','z' }

		for _,i in ipairs( chars ) do
			if glfw.get_key( window, i ) == 'press' then Keys[ i ]() end
		end
	end
end

Mouse = {}
Mouse.x = nil
Mouse.y = nil

-- bread's default camera
Camera = {}
Camera.position    = vec3( 0.0, 0.0, 0.0 )
Camera.up          = vec3( 0.0, 1.0, 0.0 )
Camera.front       = vec3( 0.0, 0.0, -1.0 )
Camera.right       = (Camera.front % Camera.up):normalize()
Camera.speed       = 1
Camera.sensitivity = 0.1
Camera.zoom        = 45.0
Camera.dt          = 0.0
Camera.yaw         = -90.0
Camera.pitch       = 0.0

-- camera vectors
function UpdateVectors()
	Camera.front = vec3( cos( rad( Camera.yaw ) ) * cos( rad( Camera.pitch ) )
			     , sin( rad( Camera.pitch ) )
			     , sin( rad( Camera.yaw ) )*cos( rad( Camera.pitch ) )
			   ):normalize()

	--Camera.right = ( Camera.front % Camera.up ):normalize()
	Camera.up 	 = ( Camera.right % Camera.front ):normalize()
	Camera.up[1] = 0
	Camera.up[3] = 0
end

function ProcessMouse( xoffset, yoffset )
	Camera.yaw   = Camera.yaw   + xoffset * Camera.sensitivity
	Camera.pitch = Camera.pitch + yoffset * Camera.sensitivity

--	if constraintPitch then
	Camera.pitch = clamp( Camera.pitch, -89.0, 89.0 )
--	end

	UpdateVectors()
end

function ProcessScroll( yoffset )
	Camera.zoom = clamp( Camera.zoom - yoffset, 1.0, 45.0 )
end

-- return a look_at vector
function Camera.view( camera )
	return look_at( camera.position, camera.position+camera.front, camera.up )
end

function TestKeys( window, key, scancode, action )
	Console( "Pressed Key: " .. key )
	if Keys[ key ] ~= nil then Keys[ key ]() end

	if key == 'q' and action == 'press' then glfw.set_window_should_close() end
end

-- key callback function
--function Keys( window, key, scancode, action )
--	if key == 'w' and action == 'press' then
--		Camera.position.y = Camera.dt*( Camera.position.y - 1 ) --* Camera.sensitivity
--	end
--
--	if key == 's' and action == 'press' then
--		Camera.position.y = Camera.position.y + 1 --* Camera.sensitivity
--	end
--
--	if key == 'q' and action == 'press'  then
--		glfw.set_window_should_close( true )
--	end
--
--	if key == 'up' and action == 'press' then
--		Camera.position = Camera.position + Camera.front*Camera.speed * Camera.dt
--	end
--
--	if key == 'down' and action == 'press' then
--		Camera.position = Camera.position - Camera.front*Camera.speed*Camera.dt
--	end
--
--	if key == 'left' and action == 'press' then
--		Camera.position.x = Camera.position.x - 1
--	end
--
--	if key == 'right' and action == 'press' then
--		Camera.position.x = Camera.position.x + 1
--	end
--end

-- compute the mvp matrix
function ComputeMVP( shader )
	local model = translate( 0.0, 0.0, 0.0 ) * scale(1.0)
	local view  = Camera:view()
	local projection = perspective( rad( Camera.zoom ), 1, 0.1, 100.0 )

	--gl.uniform_matrix4f( Uniform.model, true, model )
	--gl.uniform_matrix4f( Uniform.view, true, view )
	--gl.uniform_matrix4f( Uniform.projection, true, projection )

	gl.uniform_matrix4f( gl.get_uniform_location( shader , "model" )      , true , model )
	gl.uniform_matrix4f( gl.get_uniform_location( shader , "view" )       , true , view )
	gl.uniform_matrix4f( gl.get_uniform_location( shader , "projection" ) , true , projection )

	return model, view, projection
	--return model*view*projection
end

function Raycasting( xpos, ypos )
	local rayDirection = { xpos, ypos, -1.0 }
	Console( rayDirection[1], rayDirection[2], rayDirection[3] )
	-- convert to noramlized device coordinates
	rayDirection[1] = ( 2 * rayDirection[1] ) / 500 - 1
	rayDirection[2] = -( ( 2 * rayDirection[2] ) / 500 - 1 )

	-- convert to homogeneous ( z = - 1, w = 1 ) 
	local clipCoordinates = { rayDirection[1], rayDirection[2], rayDirection[3], 1.0 }
	clipCoordinates = glmath.tovec4( clipCoordinates )
	
	-- convert to eye space
	local projection = perspective( rad( Camera.zoom ), 1, 0.1, 100.0 )
	local invProj = glmath.inv( projection )
	local eyeCoords = invProj * clipCoordinates
	eyeCoords[3] = -1
	eyeCoords[4] = 0

	-- convert to world space
	local invView = glmath.inv( Camera:view() )
	local rayWorld = invView * eyeCoords
	rayWorld = glmath.tovec3( rayWorld )
	glmath.normalize( rayWorld )

	--os.execute( "clear" )
	Console( "[Converted]\n" .. rayWorld[1] .. " " .. rayWorld[2] .. " " .. rayWorld[3] )

	return { rayWorld[1], rayWorld[2] }
	--local denominator = glmath.dot(   )
	
--	print( ComputeSurfaceNormal( Button.vertices ) )
end
Text = {}
Text.__index = Text
Text.mesh = nil
Text.fontFace = nil

-- Create a texture fot the selected font
function TextTexture( glyph )
	local texture = gl.gen_textures()
	gl.bind_texture( '2d', texture )

	gl.texture_parameter( '2d', 'wrap s', 'clamp to edge' )
	gl.texture_parameter( '2d', 'wrap t', 'clamp to edge' )
	gl.texture_parameter( '2d', 'min filter', 'linear' )
	gl.texture_parameter( '2d', 'mag filter', 'linear' )

	gl.texture_image( '2d', 0, 'red', 'red', 'ubyte', glyph.bitmap.buffer, glyph.bitmap.width, glyph.bitmap.rows )
	gl.generate_mipmap( '2d' )

	return texture
end

function InitFont( filename, size )
	local lib = ft.init_freetype()

	local face = ft.new_face( lib, filename )
	face:set_pixel_sizes( 0, size )
	gl.pixel_store('unpack alignment', 1)

	face:select_charmap( 1 )

	face:load_char( 64, 0 )

	--local glyph = face:glyph()
	
	local totalSize = 0
	local lastLetterAdvance = 0

	for c in string.gmatch( chars, "." ) do
		face:load_char( c, ft.LOAD_RENDER )
		local glyph = face:glyph()

		local xpos = 0 + glyph.bitmap.left --* size
		local ypos = 0 - (glyph.bitmap.rows - glyph.bitmap.top) --* size

		local w = glyph.bitmap.width * size
		local h = glyph.bitmap.rows  * size

		totalSize = totalSize + w

		local vertices =
		{
			( xpos     ) /100 , ( ypos + h ) /100, 0.0,
			( xpos     ) /100 , ( ypos     ) /100, 0.0,
			( xpos + w ) /100 , ( ypos     ) /100, 0.0,

			( xpos     ) /100 , ( ypos + h ) /100, 0.0,
			( xpos + w ) /100 , ( ypos     ) /100, 0.0,
			( xpos + w ) /100 , ( ypos + h ) /100, 0.0,
		};

		local texCoords =
		{
			0.0, 0.0,
			0.0, 1.0,
			1.0, 1.0,

			0.0, 0.0,
			1.0, 1.0,
			1.0, 0.0
		};

		local indices = {
			0, 1, 3,
			1, 2, 3,
		}

		local temp = Mesh:Load2D( vertices, indices, texCoords, TextTexture( glyph ), 'text' )
		temp.scale = size
		temp.Orientation = 1

		table.insert( fontTextureTable, { c, glyph, glyph.bitmap, temp } )
	end

	local t = fontTextureTable[96][4]
	t.color = vec4( 0,0,0,0 )
	table.insert( fontTextureTable, { " ", fontTextureTable[95][2], fontTextureTable[95][3], t } )

	LineHeight = fontTextureTable[27][3].top

	lib:done()
end

-- render a string at x and y with scale
function RenderText( str, x, y, fscale )
	fscale = fscale/100
	gl.enable( "cull face" )
	gl.enable( "blend" )
	gl.blend_func( "src alpha", "one minus src alpha" )

	for _,i in ipairs( fontTextureTable ) do
		i[4].scale = fscale
	end

	local lastItemWidth = 0
	local totalSize = 0
	local lastLetterAdvance = 0

	local counter =0
	local bc = 0

	--LineHeight = LineHeight * fscale

	--Console( "CHAR	SIZE	X	XPOS	ADV	YPOS" )
	for char in string.gmatch( str, '.' ) do
		for index, item in ipairs( fontTextureTable ) do
			if char == item[1] then
				--x = x + math.floor( lastLetterAdvance * 3^ 6 ) * fscale
				x = x + lastItemWidth
				--local xpos = x + item[3].left --* fscale
				local xpos = x/2
				local ypos = y - (item[3].rows - item[3].top) --* fscale

				--if counter == 0 then lastLetterAdvance = item[2].advance.x * fscale counter = counter + 1 end
				if counter == 0 then lastLetterAdvance = item[2].advance.x counter = counter + 1 end

				--item[4].scale = fscale
				item[4].color = vec4( 1.0, 1.0, 1.0, 1.0 )
				item[4]:DrawText( { xpos, ypos, 0 }, {0,0,0} )

				lastLetterAdvance = item[2].advance.x * fscale
				lastItemWidth = item[2].bitmap.width

				--x = x + item[3].width + math.floor( item[2].advance.x / 2 ^ 6 ) * fscale
				--x = x + lastItemWidth + math.floor( lastLetterAdvance / 2 ^ 6 ) * fscale
				--x = x + item[2].advance.x
--				LastItemWidth = lastLetterAdvance * fscale
				--Console( "Last: " .. x )
				--Console( item[1] .. "	" .. item[2].bitmap.width .. "	" .. x .. "	" .. xpos .. "	" .. item[2].advance.x*fscale .. "	".. ypos )
			end
		end
	end
	gl.disable( 'blend' )
	--Console( "Total Text Size: " .. totalSize )
end

-- create the main loop function
function Loop( callback )
	-- load the font
	InitFont( "assets/HasklugFont.otf", 48 )

	--Framebuffer()

	-- compute the delta time
	local lastFrame = 0.0
	local t = glfw.get_time()
	Camera.dt = t - lastFrame

	-- main loop
	while not glfw.window_should_close( window ) do
		--gl.bind_framebuffer( 'draw read', fbo )
		glfw.poll_events()
		gl.clear_color( 0.2, 0.2, 0.2, 0.2 )
		gl.clear( "color", "depth" )
		--gl.viewport( 0, 0, WindowWidth, WindowHeight )

		ProcessInput()

		-- execute the draw calls
		collectgarbage( "collect" )
		callback()

		--gl.bind_framebuffer( 'draw read', 0 )
		gl.disable( "depth test" )
		--gl.clear('color')
		glfw.swap_buffers( window )

		ConsolePrint()

--		for _,i in ipairs ( fontTextureTable ) do
--			i[4]:Clean()
--		end
	end
end
function Framebuffer()
	local fbo = gl.new_framebuffer( 'draw read' )

	gl.bind_framebuffer( 'draw read', fbo )

	
end

Button = {}
Button.__index = Button
Button.mesh = {}
Button.text  = nil
Button.sizex = 0
Button.sizey = 0
Button.Orientation = 1.0
Button.player = false
Button.id = nil
--Button.vertices  = { 0,0,0, 2,0,0, 0,1,0, 2,1,0 }
--Button.indices   = { 1,2,0,1,3,2 }
--Button.texCoords = { 0,0, 1,0, 1,1, 0,1 }
--Button.color     = { 0.1, 0.1, 0.1, 1 }
--Button.position  = { 0  , 0  , 0  , 0 }
--Button.vao       = nil
--Button.shader    = nil

function Distance( a, b )
	return math.sqrt( math.pow( ( a[1] - b[1] ), 2 ) + math.pow( ( a[2] - b[2] ), 2 ) )
end

function Button:Collision2D()
	local box = self.mesh.box
	for _,i in ipairs( CollisionItems ) do
		if self.id ~= i.id then
			local luigi = i.mesh.box
			local luigimiddle = { ( math.abs( luigi[2][1] ) + math.abs( luigi[1][1] ) ) / 2, ( math.abs( luigi[4][2] ) + math.abs( luigi[3][2] ) ) / 2 }
			local boxmiddle   = { ( math.abs( box[2][1] ) + math.abs( box[1][1] ) ) / 2, ( math.abs( box[4][2] ) + math.abs( box[3][2] ) ) / 2 }
			--if( box[1][1] > luigi[1][1] and box[2][1] < luigi[2][1] ) then
			--if ( math.abs( box[1][1] ) >= math.abs( luigi[1][1] * 0.5 ) and math.abs( box[1][1] ) <= math.abs( luigi[2][1] ) ) then

			Console( "Mario: " )
			Console( self.mesh.position )

			Console( "Luigi: " )
			Console( i.mesh.position )

			if box[2][1] >= luigi[1][1] and 
			   box[1][1] <= luigi[2][1] and 
			   box[4][2] >= luigi[1][2] and
			   box[1][2] <= luigi[4][2]
			then
				Collided = true
				Console( "Collided" )
				self.mesh.color = { 1.0, 0.0, 0.0, 0.2 }

				if PreviousButton == 'w' then PlayerPosition[2] = PlayerPosition[2] - 0.1 Collided = true end
				if PreviousButton == 's' then PlayerPosition[2] = PlayerPosition[2] + 0.1 Collided = true end
				if PreviousButton == 'a' then PlayerPosition[1] = PlayerPosition[1] + 0.1 Collided = true end
				if PreviousButton == 'd' then PlayerPosition[1] = PlayerPosition[1] - 0.1 Collided = true end
			else
				Collided = false
				self.mesh.color = DefaultColor
			end

		

--			if Distance( luigimiddle, boxmiddle ) <= ( math.abs( DefaultBox[1][1] ) + math.abs( DefaultBox[2][1] ) ) then
--				Console( "Distance: " .. Distance( luigimiddle, boxmiddle ) )
--				Console( "Middle: " .. ( math.abs( DefaultBox[1][1] ) + DefaultBox[2][1] ) )
--				Console( "Luigi Middle: " .. unpack( luigimiddle ) )
--				Console( "Collided" )
--			else
--				Console( "Distance: " .. Distance( luigimiddle, boxmiddle ) )
--				Console( "Middle: " .. ( math.abs( DefaultBox[1][1] ) + DefaultBox[2][1] ) )
--				Console( "Luigi Middle: " .. unpack( luigimiddle ) )
--				Console( "Not Collided" )
--			end
			
		end
	end
end

function Button:New( texture )
	local button = {}
	setmetatable( button, Button )

	local temp = {}
	setmetatable( temp, Mesh )

	ButtonCounter = ButtonCounter + 1

	button.mesh = Mesh:Load2D( { 1,1,0, 1,-1,0, -1,-1,0, -1,1,0 }, { 0,1,3,1,2,3 }, { 1,1, 1,0, 0,0, 0,1 }, texture )
	button.mesh.scale = 1.0
	button.mesh.position = { 0, 0, 0 }
	button.mesh.Orientation = 1.0
	button.Orientation = 1.0
	button.mesh.box = { {-1,-1}, { 1,-1 } , { 1,1 } , { -1,1 } }
	button.id = ButtonCounter
	
	table.insert( CollisionItems, button )

	return button
end

function Button:Draw( position )
	if self.player then self.mesh.Orientation = PlayerOrientation end

	self.mesh.position[1] = position[1]
	self.mesh.position[2] = position[2]
	self.mesh.position[3] = position[3]

	self.mesh.box[1] = { self.mesh.scale * ( self.mesh.position[1] + DefaultBox[1][1] ), self.mesh.scale * ( self.mesh.position[2] + DefaultBox[1][2] ) } -- ll
	self.mesh.box[2] = { self.mesh.scale * ( self.mesh.position[1] + DefaultBox[2][1] ), self.mesh.scale * ( self.mesh.position[2] + DefaultBox[2][2] ) } -- lr
	self.mesh.box[3] = { self.mesh.scale * ( self.mesh.position[1] + DefaultBox[3][1] ), self.mesh.scale * ( self.mesh.position[2] + DefaultBox[3][2] ) } -- tl
	self.mesh.box[4] = { self.mesh.scale * ( self.mesh.position[1] + DefaultBox[4][1] ), self.mesh.scale * ( self.mesh.position[2] + DefaultBox[4][2] ) } -- tr

	self.mesh:Draw2D( self.mesh.position, { 0,0,0 } )

	if( self.text ~= nil ) then
		local textScale = 0.5
		local uiScale = textsizex / 100
		local model = scale( textsizex/100 )
		--local model = scale( 1.0 )
		gl.uniform_matrix4f( gl.get_uniform_location( tshader, 'model' ), true, model )
		gl.uniform( gl.get_uniform_location( tshader, 'position' ), 'float', gl.flatten( { ( 1.0 ) * ( uiScale ) - ( textsizex/180 ) + uiScale*( self.position[1] ), ( 0.5 ) * ( uiScale ) - ( textsizey/180 ) + uiScale*( self.position[2] ), 0, 0 } ) )
		RenderText( self.text, 0.0, 1, 1.0 )
	end
end

--function Button:New1( text, sizex, sizey, position )
--	local button = {}
--	setmetatable( button, Button )
--	button.text = text
--	button.sizex = sizex
--	button.sizey = sizey
--	--button.position = position
--
--	local temp = {}
--	setmetatable( temp, Mesh )
--	button.mesh = temp.Load2D( { 0,0,0, 2,0,0, 0,1,0, 2,1,0 }, { 1,2,0,1,3,2 }, { 0,0, 1,0, 1,1, 0,1 }, texture )
--	button.mesh.position = position
--	--button.vao = Load( Button.vertices, Button.indices, Button.texCoords )
--	--button.shader = Shader( "shaders/vertexButton.glsl", "shaders/fragButton.glsl", '2d' )
--	--button.shader = Shader( '2d' )
--
--	return button
--end

--function Button:Draw()
--	--ComputeMVP()
--	gl.use_program( self.shader )
--
--	local textsizex, textsizey = getSize( self.text, 1.3 )
--
--	local textScale = 0.5
--
--	local uiScale = textsizex / 100
--	local model = scale( textsizex/100 )
--	gl.uniform_matrix4f( gl.get_uniform_location( self.shader, 'model' ), true, model )
--	gl.uniform( gl.get_uniform_location( self.shader, 'position' ), 'float', gl.flatten( self.position ) )
--	gl.uniform( gl.get_uniform_location( self.shader, 'color' ), 'float', gl.flatten( self.color ) )
--	gl.bind_vertex_array( self.vao )
--	--gl.bind_texture( '2d', self.texture )
--	gl.draw_elements( 'triangle strip', 1600*8, 'uint', 0 )
--	gl.unbind_vertex_array()
--	gl.disable( "depth test" )
--	UseShader( '2d' )
--	local model = scale( 1.0 )
--	gl.uniform_matrix4f( gl.get_uniform_location( tshader, 'model' ), true, model )
--	gl.uniform( gl.get_uniform_location( tshader, 'position' ), 'float', gl.flatten( { ( 1.0 ) * ( uiScale ) - ( textsizex/180 ) + uiScale*( self.position[1] ), ( 0.5 ) * ( uiScale ) - ( textsizey/180 ) + uiScale*( self.position[2] ), 0, 0 } ) )
--	RenderText( self.text, 0.0, 1, 1.0 )
--end

function Console( a )
	table.insert( Log, a )	
end

function ConsolePrint()
	for _,i in ipairs( Log ) do
		if type( i ) == "table" then
			print( unpack( i ) )
		else
			print( i )
		end
	end

	os.execute( 'clear' )

	for _,i in ipairs( Log ) do
		if type( i ) == "table" then
			print( unpack( i ) )
		else
			print( i )
		end
	end

	Log = {}
end

function Collision( M )
	print( "Look: " )
	for _,i in ipairs( Objects ) do
		-- minx, maxx, miny, maxy, minz, maxz
--		if( M.box[1] = i.box[1],  ) then
--		print( i.box )
--		end
	end
end

function ConvexHull( vertices )
	local lowX = 0; local lowY = 0
	local P = {}
	local angles = {}
	for _,i in ipairs( vertices ) do
		--print( i[1], i[2], i[3] )

		-- first must find P( lowest Y and lowest X )
		local vec = { i[1], i[2] }
		if i[2] < lowY then
			lowX = i[1]
			lowY = i[2]
			P = vec 
		end

		if i[2] == lowY then
			if i[1] < lowX then
				P = vec
			end
		end
	end

	glmath.tovec2( P )

	for _,i in ipairs( vertices ) do
		local vec = { i[1], i[2] }
		--glmath.tovec2( vec )
		--table.insert( angles, { vec, math.acos( ( P * vec ) ) / ( math.sqrt( ( vec * vec ) ) * ( math.sqrt( ( P * P ) ) ) ) } )
		table.insert( angles, { vec, ( math.atan2( vec[1], vec[2] ) ) } )
	end

	table.sort( angles, function( a, b ) return a[2] < b[2] end )

	local hull = {}

	print( "ha: " .. P[1] .. P[2] )

	local last = P
	local candidate = nil

	for index,tuple in ipairs( angles ) do
		if angles[index+2] ~= nil then candidate = angles[index+2] else table.insert( hull, P ) break end
		print( "candidate: " .. candidate[1][1] .. candidate[1][2] )
		if( Turn( last, tuple[1], candidate[1] ) ) then table.insert( hull, candidate[1] ) end
		last = tuple[1]
	end

	--table.insert( angles, P, 0 )
	--for _,i in ipairs( angles ) do
	local atual, proximo, futuro
	for i=1,#angles do
		-- the right and left turn thing
		--ShowTable( angles[i][1] )

--		if angles[i+2][1] ~= nil and angles[i+1][1] ~= nil then
--			if Turn( angles[i][1], angles[i+1][1], angles[i+2][1] ) <= 0 then
--				table.insert( hull, angles[i][1] )
--			else
--				--angles[i+1][1] = nil
--				table.remove( angles, i+1 )
--			end
--		end
	end

	for i=1,#hull do
		print( hull[i][1], hull[i][2] )
	end

	Testhull = hull
	return hull
end

function Cross( p1, p2 )
	return p1[1] * p2[2] - p2[1] * p1[2]
end

function Turn( p1, p2, p3 )
	--local ret = glmath.tovec2( { p2[1] - p1[1] , p2[2] - p1[2] } ) - glmath.tovec2( { p3[1] - p1[1], p3[2] - p1[2] } )
	print( "==========" )

	ShowTable( p1 )
	ShowTable( p2 )
	print( Cross( glmath.tovec2( { p2[1] - p1[1] , p2[2] - p1[2] } ), glmath.tovec2( { p3[1] - p1[1], p3[2] - p1[2] } ) ) == 1 )

	if( Cross( glmath.tovec2( { p2[1] - p1[1] , p2[2] - p1[2] } ), glmath.tovec2( { p3[1] - p1[1], p3[2] - p1[2] } ) ) == 1 ) then 
		print( "left turn" )
	else 
		print( "right turn" ) 
	end

	print( "==========" )
	return Cross( glmath.tovec2( { p2[1] - p1[1] , p2[2] - p1[2] } ), glmath.tovec2( { p3[1] - p1[1], p3[2] - p1[2] } ) )
end

function CompareTables( t1, t2 )
	for index,i in ipairs( t1 ) do
		if( t1[1] == t2[1] and t1[2] == t2[2] ) then return 1
		else return 0 end
	end
end

function ShowTable( t ) print( t[1], t[2] ) end

function RayCasting( point, polygon )
	local n = #polygon
	local count = 0
	local x = point[1]
	local y = point[2]

	for i=1, n-1 do
		local Aside = { polygon[i][1], polygon[i][2] }
		local Bside = { polygon[i+1][1], polygon[i+1][2] }
		local x1 = Aside[1]
		local x2 = Bside[1]
		local y1 = Aside[2]
		local y2 = Bside[2]

		if ( ( y < y1 ) ~= ( y < y2 ) ) then 
			if  ( x < ( ( x2-x1 ) * ( y - y1 ) / ( y2 - y1 ) + 1 ) )  then
				count = count + 1
			end
		end
	end

	print( "Count: " .. count )
	if count % 2 == 0 then return true else return 0 end
end


function Sleep( time )
	glfw.sleep( time )
end

LastTime = 0
function CurrentTime( delta )
	local time = glfw.now()
	local dt = glfw.since( delta )
	LastTime = time

	return dt
end

function ComputeSurfaceNormal( vertices )
	local normal = { 0, 0, 0 }

	for i=1,#vertices do
		local Current = vertices[i]
		local Next    = vertices[( i+1 ) % i ]

		normal[1] = normal[1] + ( Current[2] - Next[2] ) * ( Current[3] + Next[3] )
		normal[2] = normal[2] + ( Current[3] - Next[3] ) * ( Current[1] + Next[1] )
		normal[3] = normal[3] + ( Current[1] - Next[1] ) * ( Current[2] + Next[2] )

		return glmath.normalize( normal )
	end
end

Mesh               = {}
Mesh.__index       = Mesh
Mesh.vertices      = {}
Mesh.indices       = {}
Mesh.uvs           = {}
Mesh.normals       = {}
Mesh.position      = {}
Mesh.box           = {}
Mesh.animations    = {}
Mesh.Orientation   = nil
Mesh.texture       = nil
Mesh.shader        = nil
Mesh.bump          = nil
Mesh.verticesCount = nil
Mesh.vao           = nil
Mesh.vbo           = nil
Mesh.color         = nil
Mesh.scale         = nil

function Mesh:SetUniform( uniform, value )
	for _,i in ipairs( self.shader[2] ) do
		if i[1] == uniform then
			gl.uniform( i[2], i[3], gl.flatten( value ) )
		end
	end
end

function Mesh:LoadOBJ( filename, texture, bump )
	--local scene, err = ai.import_file( filename, "triangulate", "join identical vertices", "sort by p type" )
	local scene, err = ai.import_file( filename, 'triangulate', 'flip uvs', 'gen smooth normals', 'calc tangent space', 'join identical vertices' )
	assert( scene, err )

	local meshes = scene:meshes()

	local vertices   = {}
	local texCoords  = {}
	local normals    = {}
	local tangents   = {}
	local bitangents = {}

	for index,i in ipairs( scene:meshes() ) do
		print( index, i )
	end

	local hasTex = meshes[1]:has_texture_coords(1)

	for i = 1, meshes[1]:num_vertices() do
		--vertices[i] = { meshes[1]:position(i) }
		table.insert( vertices, { meshes[1]:position(i) } )
		local u, v
		if hasTex then u, v = meshes[1]:texture_coords(1, i) else u, v = 0.0, 0.0 end
		--texCoords[i] = { meshes[1]:texture_coords(1, i) }
		--texCoords[i] = { u, v }
		table.insert( texCoords, { u, v } )
		if meshes[1]:has_normals() then table.insert( normals, { meshes[1]:normal(i) } ) end
		table.insert( tangents,   { meshes[1]:tangent(i)   } )
		table.insert( bitangents, { meshes[1]:bitangent(i) } )
	end

	local indices = meshes[1]:all_indices( true )

	local count = #indices/gl.sizeof( 'uint' )

	--return Load( vertices, indices, texCoords ), Texture( a ), count

	local mesh = {}
	setmetatable( mesh, Mesh )

	-- create the buffers
	local vao  = gl.gen_vertex_arrays()
	local vbo  = gl.gen_buffers()
	local ebo  = gl.gen_buffers()
	local tvbo = gl.gen_buffers()
	local nvbo = gl.gen_buffers()
	local tangentVbo   = gl.gen_buffers()
	local bitangentVbo = gl.gen_buffers()

	-- bind the vertex array
	gl.bind_vertex_array( vao )

	-- vertices buffer
	gl.bind_buffer( 'array', vbo )
	gl.buffer_data( 'array', gl.pack( 'float', vertices ), 'static draw' )
	gl.vertex_attrib_pointer( 0, 3, 'float', false, 3*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array( 0 )
	gl.unbind_buffer( 'array' )

	-- texture coordinates buffer
	gl.bind_buffer( 'array', tvbo )
	gl.buffer_data( 'array', gl.pack( 'float', texCoords ), 'static draw' )
	gl.vertex_attrib_pointer( 1, 2, 'float', false, 2*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array(1)
	gl.unbind_buffer( 'array' )

	-- normals buffer
	gl.bind_buffer( 'array', nvbo )
	gl.buffer_data( 'array', gl.pack( 'float', normals ), 'static draw' )
	gl.vertex_attrib_pointer( 2, 3, 'float', false, 3*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array( 2 )
	gl.unbind_buffer( 'array' )

	-- tangents buffer
	gl.bind_buffer( 'array', tangentVbo )
	gl.buffer_data( 'array', gl.pack( 'float', tangents ), 'static draw' )
	gl.vertex_attrib_pointer( 3, 3, 'float', false, 3*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array( 3 )
	gl.unbind_buffer( 'array' )

	-- bittangents buffer
	gl.bind_buffer( 'array', bitangentVbo )
	gl.buffer_data( 'array', gl.pack( 'float', bitangents ), 'static draw' )
	gl.vertex_attrib_pointer( 4, 3, 'float', false, 3*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array( 4 )
	gl.unbind_buffer( 'array' )

	-- indices buffer
	ebo = gl.new_buffer('element array') -- element buffer object
	gl.buffer_data('element array', gl.pack('uint', indices), 'static draw')

	-- unbind the vertex array
	gl.unbind_vertex_array()

	-- compute a simple 3d bounding box
	local minx = 0
	local maxx = 0
	local miny = 0
	local maxy = 0
	local minz = 0
	local maxz = 0

	for _,i in ipairs( vertices ) do
		if i[1] < minx then minx = i[1] end
		if i[1] > maxx then maxx = i[1] end
		if i[2] < miny then miny = i[2] end
		if i[2] > maxy then maxy = i[2] end
		if i[3] < minz then minz = i[3] end
		if i[3] > maxz then maxz = i[3] end
	end
		
	print( "Box: " .. minx .. maxx .. miny .. maxy .. minz .. maxz )
	mesh.shader        = Shader( 'cel' )
	mesh.vao           = vao
	mesh.vbo           = vbo
	mesh.vertices      = vertices
	mesh.indices       = indices
	mesh.uvs           = texCoords
	mesh.normals       = normals
	mesh.box           = glmath.box3( minx, maxx, miny, maxy, minz, maxz )
	mesh.verticesCount = #indices/gl.sizeof( 'uint' )
	table.insert( mesh.texture, #Textures )
	mesh.bump          = bump
	mesh.color         = DefaultColor

	table.insert( Objects, mesh )
	return mesh
end

function Mesh:DynamicLoad( vertices, indices, texCoords, texture )
	local mesh = {}
	setmetatable( mesh, Mesh )

	-- create the buffers
	mesh.vao           = gl.gen_vertex_arrays()
	mesh.vbo           = gl.gen_buffers()
	mesh.ebo           = gl.gen_buffers()
	mesh.tvbo          = gl.gen_buffers()
	mesh.shader        = Shader( '2d' )
	mesh.vertices      = vertices
	mesh.indices       = indices
	mesh.uvs           = texCoords
	mesh.verticesCount = #indices/gl.sizeof( 'uint' )
	--table.insert( mesh.texture, texture )
	mesh.texture = texture
	mesh.color         = DefaultColor
	mesh.position = { 0, 0, 0 }

	-- bind the vertex array
	gl.bind_vertex_array( mesh.vao )

	-- vertices buffer
	gl.bind_buffer( 'array', mesh.vbo )
	gl.buffer_data( 'array', gl.pack( 'float', vertices), 'dynamic draw' )
	gl.vertex_attrib_pointer( 0, 3, 'float', false, 3*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array( 0 )
	gl.unbind_buffer( 'array' )

	-- indices buffer
	mesh.ebo = gl.new_buffer('element array') -- element buffer object
	gl.buffer_data('element array', gl.pack('uint', indices), 'static draw')

	-- texture coordinates buffer
	gl.bind_buffer( 'array', mesh.tvbo )
	gl.buffer_data( 'array', gl.pack( 'float', texCoords ), 'static draw' )
	gl.vertex_attrib_pointer( 1, 2, 'float', false, 2*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array(1)
	gl.unbind_buffer( 'array' )

	-- unbind the vertex array
	gl.unbind_vertex_array()

	return mesh
end

function Mesh:UpdateBuffer( vertices )
	gl.bind_vertex_array( self.vao )
	gl.bind_buffer( 'array', self.vbo )
	gl.buffer_data( 'array', gl.pack( 'float', vertices ), 'dynamic draw' )

	gl.unbind_vertex_array( self.vao )
end

function Mesh:AnimationTable( filename )
	local file = io.open( filename, 'rb' )

	local lines = {}

	if file then
		for line in file:lines() do
			table.insert( lines, line )
		end
	end

	local header = nil
	local temp = {}

	for _,i in ipairs( lines ) do
		if i:find( '%[.*%]' ) then
			if header then
				table.insert( self.animations, { header, temp } )
				temp = {}
			end
			header = i:match( '%w+' )
		else
			if i ~= '' then table.insert( temp, Texture( i ) ) end
		end
	end

	table.insert( self.animations, { header, temp } )
end

function Mesh:PlayAnimation( name, speed )
	local size = nil
	local anim
	for _,i in ipairs( self.animations ) do
		if name == i[1]	then size = #i[2]; anim = i end
	end

	Console( math.floor( CurrentTime( 1 ) % #anim[2] ) )

	gl.uniform( gl.get_uniform_location( self.shader[1], "Orientation" ), 'float', -self.Orientation )
	self.texture = anim[2][ math.floor( speed * CurrentTime( 1 ) % #anim[2] ) + 1 ]
	--self.texture = self.animations[][ math.floor( CurrentTime( 1 ) % size ) ]
end

function Mesh:Load2D( vertices, indices, texCoords, texture, shaderType )
	local mesh = {}
	setmetatable( mesh, Mesh )

	
	-- create the buffers
	mesh.vao           = gl.gen_vertex_arrays()
	mesh.vbo           = gl.gen_buffers()
	mesh.ebo           = gl.gen_buffers()
	mesh.tvbo          = gl.gen_buffers()
	if shaderType then mesh.shader = Shader( shaderType ) else mesh.shader = Shader( '2d' ) end
	mesh.vertices      = vertices
	mesh.indices       = indices
	mesh.uvs           = texCoords
	mesh.verticesCount = #indices/gl.sizeof( 'uint' )
	--table.insert( mesh.texture, texture )
	mesh.texture = texture
	mesh.color         = DefaultColor

	-- bind the vertex array
	gl.bind_vertex_array( mesh.vao )

	-- vertices buffer
	gl.bind_buffer( 'array', mesh.vbo )
	gl.buffer_data( 'array', gl.pack( 'float', vertices), 'static draw' )
	gl.vertex_attrib_pointer( 0, 3, 'float', false, 3*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array( 0 )
	gl.unbind_buffer( 'array' )

	-- indices buffer
	mesh.ebo = gl.new_buffer('element array') -- element buffer object
	gl.buffer_data('element array', gl.pack('uint', indices), 'static draw')

	-- texture coordinates buffer
	gl.bind_buffer( 'array', mesh.tvbo )
	gl.buffer_data( 'array', gl.pack( 'float', texCoords ), 'static draw' )
	gl.vertex_attrib_pointer( 1, 2, 'float', false, 2*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array(1)
	gl.unbind_buffer( 'array' )

	-- unbind the vertex array
	gl.unbind_vertex_array()

	return mesh
end

function Mesh:Draw2D( position, lpos )
	gl.enable( 'depth test' )
	gl.depth_func( 'less' )
	gl.enable( 'line smooth' )

	gl.use_program( self.shader[1] )

	--local model, view, projection = ComputeMVP( self.shader[1] )
	gl.uniform_matrix4f( gl.get_uniform_location( self.shader[1] , "model" ), true , scale( self.scale ) )
	gl.uniform( gl.get_uniform_location( self.shader[1], "Orientation" ), 'float', self.Orientation )
	self.position = position
	self:SetUniform( 'position', position )
	self:SetUniform( 'color',  self.color )
	self:SetUniform( 'lightPos',  lpos )
	--self:SetUniform( 'Orientation', PlayerOrientation )
--	print( 'Orientation: ' .. PlayerOrientation )

	gl.bind_vertex_array( self.vao )

--	for i=1,#self.texture do
--		gl.active_texture( i-1 )
--		gl.bind_texture( '2d', Textures[self.texture[i]] )
--		print( #self.texture )
--	end

	--gl.active_texture( 0 )
	gl.bind_texture( '2d', self.texture )

	gl.draw_elements( 'triangles', 1600*self.verticesCount, 'uint',  0 )
	gl.unbind_vertex_array()
	gl.disable( "depth test" )
end

function Mesh:DrawText( position, lpos )
	gl.enable( "cull face" )
	gl.enable( "blend" )
	gl.blend_func( "src alpha", "one minus src alpha" )

	gl.disable( "depth test" )
	gl.use_program( self.shader[1] )

	--local model, view, projection = ComputeMVP( self.shader[1] )
	gl.uniform_matrix4f( gl.get_uniform_location( self.shader[1] , "model" ), true , scale( self.scale ) )
	gl.uniform( gl.get_uniform_location( self.shader[1], "Orientation" ), 'float', self.Orientation )
	self.position = position
	self:SetUniform( 'position', position )
	self:SetUniform( 'color',  self.color )
	self:SetUniform( 'lightPos',  lpos )
	--self:SetUniform( 'Orientation', PlayerOrientation )
--	print( 'Orientation: ' .. PlayerOrientation )

	gl.bind_vertex_array( self.vao )

--	for i=1,#self.texture do
--		gl.active_texture( i-1 )
--		gl.bind_texture( '2d', Textures[self.texture[i]] )
--		print( #self.texture )
--	end

	--gl.active_texture( 0 )
	gl.bind_texture( '2d', self.texture )

	gl.draw_arrays( 'triangles', 0, 18 )
	gl.unbind_vertex_array()
	--gl.disable( "depth test" )

	gl.disable( "cull face" )
	gl.disable( "blend" )
end

function Mesh:DrawLine()
	gl.enable( 'depth test' )
	gl.depth_func( 'less' )
	gl.enable( 'line smooth' )

	gl.use_program( self.shader[1] )

	gl.uniform_matrix4f( gl.get_uniform_location( self.shader[1] , "model" ), true , scale( self.scale ) )
	--gl.uniform( gl.get_uniform_location( self.shader[1], "Orientation" ), 'float', self.Orientation )
	--self.position = position
	self:SetUniform( 'position', self.position )
	self:SetUniform( 'color',  self.color )
	self:SetUniform( 'lightPos',  LightPos )

	gl.enable( 'line smooth' )
	gl.hint( 'line smooth', 'nicest' )
	gl.line_width( 3 )
	gl.point_size( 10 )
	gl.point_parameter( 'fade threshold size', 0.1 )

	gl.bind_vertex_array( self.vao )
	gl.bind_texture( '2d', self.texture)
	gl.draw_elements( 'line loop', 1600*self.verticesCount, 'uint',  0 )
	gl.unbind_vertex_array()
	gl.disable( "depth test" )
end

function Mesh:Draw( position, lpos )
	gl.enable( 'depth test' )
	gl.depth_func( 'less' )
	gl.enable( 'line smooth' )

	gl.use_program( self.shader[1] )

	local model, view, projection = ComputeMVP( self.shader[1] )
	self.position = position
	self:SetUniform( 'position', position )
	self:SetUniform( 'color',  self.color )
	self:SetUniform( 'lightPos',  lpos )

	gl.bind_vertex_array( self.vao )

	--gl.uniformi( gl.get_uniform_location( shader, 'Texture1'  ), 0)
	--gl.uniformi( gl.get_uniform_location( shader, 'normalMap' ), 1)

	for i=0,#self.texture do
		gl.active_texture( i )
		gl.bind_texture( '2d', Textures[i+1] )
	end

	gl.draw_elements( 'triangles', 1600*self.verticesCount, 'uint',  0 )
	gl.unbind_vertex_array()
	gl.disable( "depth test" )
end

function Mesh:Clean()
	gl.delete_buffers( self.vao, self.vbo, self.tvbo, self.ebo )
	self.texture = nil
	gl.clean_program( self.shader[1] )
	self = nil

	Textures = nil
	collectgarbage( "collect" )
end

