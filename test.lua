require( "bread" )
local gl = require( 'moongl' )
local glfw = require( 'moonglfw' )
local glmath = require( 'moonglmath' )
local vec3 = glmath.vec3
Init()

local vertices = {
	0,0,0,
	1,0,0,
	0,1,0,
	1,1,0,
}

local indices = {
	1, 2, 0,
	1, 3, 2,
}

local texCoords = {
	0,0,
	1,0,
	1,1,
	0,1
}

--local triangle = Load( vertices, indices, texCoords )
--local texture  = Texture( "assets/blueprint.png" )

--local t  = Mesh:Load( vertices, indices, texCoords, Texture( "assets/blueprint.png" ) )
--local t1 = Mesh:Load( vertices, indices, texCoords, Texture( "assets/wood.png" ) )

--local cube, cubeTex, count = LoadObj( "assets/model.obj", "assets/facade.jpg" )
--local monkey, monkeyTex, monkeyCount = LoadObj( "assets/model.obj", "assets/blueprint.png" )

local terrain = Mesh:LoadOBJ( "assets/terrain.obj", Texture( "assets/mario.png" ), "assets/mario.png" )
local monkey = Mesh:LoadOBJ( "assets/model.obj", Texture( "assets/mario.png" ), Texture( "assets/mario.png" ) )

--local button = Button:new( "Salve Maria!", 10, 10, { 1, 1, 1, 0 } )

--local lastTime = 0
Camera.position = vec3( 0, 3, 9 )
Camera.yaw = -45.0
--local hull = ConvexHull( monkey.vertices )
--monkey.vertices = hull

Collision()

local b = Button:New( Texture( "assets/mario.png" ) )
b.mesh.scale = 0.2

function MainLoop()
--	--t:Draw( { 1.0, 1.0, 2.5, 0 } )
--	--t1:Draw( { 2, 0.5, 2.5, 0 } )
--	--Draw( triangle, texture, 1600 )
--
--	--Draw( cube, cubeTex, count )
--	--Draw( monkey, monkeyTex, monkeyCount )
	terrain:Draw( { 0.0, 0.0, 0.0 }, { 0.0, 300, 1000*math.sin( CurrentTime() ) } )
	terrain.color = {0, 1, 0.2, 1}
	terrain:SetUniform( 'color', {0, 1, 0.2, 1} )
	--monkey:Draw(  { 1.0*math.sin( time )+math.random(), 1.0*time, 0.0 }, { 0.0, 300.0, 300 } )
	monkey:Draw(  { 1.0, 1.0, 0.0 }, { 0.0, 300.0, 300 } )
	monkey.color = {0, 1, 0.2, 1}

	b:Draw()

	--button.position = { 0.0, 0.0, 0.0, 0.0 }
	--button.scale = 0.1
--	for i=1,#button.vertices do
--		button.vertices[i] = button.vertices[i]/4
--	end
	--button:draw()
	--UseShader( "2d" )
	--RenderText( "Viva Cristo Rei!", -25, 5, 1.3  )
end

Loop( MainLoop )
