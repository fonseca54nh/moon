require( 'bread' )

Init()

local background = Button:New( Texture( 'assets/background.png' ) )
background.mesh.scale = 1.0

local fishingButton = Button:New( Texture( 'assets/sardine.png' ) )
fishingButton.mesh.scale = 0.2
fishingButton.mesh.position = { 4.0, 2.5, 0.0 }

local marketButton = Button:New( Texture( 'assets/rainbowTrout.png' ) )
marketButton.mesh.scale = 0.2
marketButton.mesh.position = { 4.0, 1.5, 0.0 }


function MainLoop()
	fishingButton:Draw()
	marketButton:Draw()
	background:Draw()
end

Loop( MainLoop )
