#version 330 core

layout(location=0) in vec3 vPosition;
layout(location=1) in vec2 vTexCoords;
layout(location=2) in vec3 vNormals;
layout(location=3) in vec3 vTangents;
layout(location=4) in vec3 vBitangents;

out vec2 TexCoord;
out mat4 pvm;
out vec3 pos;
out vec3 normals;

uniform vec3 theta;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform vec3 position;

//uniform float Time;
uniform float shaderType;
uniform float Orientation;

out float st;

out VSOUT 
{
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} vsOut;

float fresnel( float amount )
{
	//return pow((1.0 - clamp(dot(normalize(vNormals), normalize(view)), 0.0, 1.0 )), amount);
	//return pow((1.0 - clamp(dot(normalize(vNormals), vNormals), 0.0, 1.0 )), amount);
	//return pow( ( 1.0 - clamp( vPosition.x, 0.2, 1.0 ), amount ) );
	return pow( vPosition.z, amount );
}

void main() 
{
    TexCoord = vTexCoords;
    st = shaderType;
    if( shaderType == 3 || shaderType == 4 ) 
    { 
	    gl_Position = model * vec4( ( vPosition + position ), 1.0 );
	    TexCoord.x = Orientation*TexCoord.x;
    }
    else { gl_Position = projection * view * model * vec4( ( vPosition + position ), 1.0 ); }
    //gl_Position = projection * view * model * vec4( ( vPosition + position ), 1.0 );

    pvm = projection * view * model;
    //pos = vPosition + position;
    //normals = mat3( transpose( inverse( model ) ) ) * vNormals;
    normals = vNormals;

    mat3 normalMat = transpose( inverse( mat3( model ) ) );
    vec3 T = normalize( normalMat * vTangents );
    vec3 N = normalize( normalMat * vNormals );
    T = normalize( T - dot( T, N ) * N );
    vec3 B = cross( N, T );

    mat3 TBN = transpose( mat3( T, B, N ) );
    pos = TBN * ( vPosition + position );

    vsOut.TangentLightPos = TBN * vec3( 0.0, 1.0, 0.0 );
    vsOut.TangentViewPos  = TBN * vec3( 0.0, 0.0, 5.0 );
    vsOut.TangentFragPos  = TBN * mat3( model ) * ( vPosition + position );

} 
