#version 330 core

in vec2 TexCoord;

uniform sampler2D Texture1;
uniform sampler2D Texture2;

void main()
{
    gl_FragColor = vec4( 1.0, 1.0, 1.0, 1.0 ) * texture(Texture1, TexCoord).r;
}
