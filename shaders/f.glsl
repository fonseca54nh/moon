#version 330 core

in vec2 TexCoord;

in mat4 pvm;
in vec3 pos;
in vec3 normals;
in float st;

// Uniforms
uniform sampler2D Texture1;
uniform sampler2D normalMap;
uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec4 color;

in VSOUT
{
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} vsOut;

float celShader()
{
    vec3 nn     = normalize( normals );
    vec3 lDir   = normalize( pos - lightPos );
    vec3 eyeDir = normalize( -pos );
    vec3 rDir   = normalize( reflect( lDir, nn ) );

    float spec    = max( dot( rDir, eyeDir ), 0.0 );
    float diffuse = max( dot( -lDir, nn ), 0.0 );

    float intensity = 0.6 * diffuse + 0.4 * spec;

    if( intensity > 0.9 )      intensity = 1.1;
    else if( intensity > 0.7 ) intensity = 0.9;
    else if( intensity > 0.5 ) intensity = 0.5;
    else intensity = 0.3;

    return intensity;
}

vec4 diffuseShader()
{
    // Normals
    //vec4 nnormals = normalize( normals );
    vec3 nnormals = normalize( texture( normalMap, TexCoord ).rgb * 2.0 - 1.0 );

    // Ambient
    float ambientStrength = 1.5;
    vec3 ambient = ambientStrength * lightColor;

    // Diffuse
    //vec4 lDir = normalize( lightPos - vec4( pos ) ); // without normal map TBN
    vec3 lDir = normalize(  vsOut.TangentLightPos - vsOut.TangentFragPos ); // with normal map TBN
    //vec4 vDir = normalize( pvm * vec4( pos ) );      // without normal map TBN
    vec3 diffuse  = texture( Texture1, TexCoord ).rgb * max( dot( nnormals, lDir ), 0.0 );

    // Specular
    float specularStrength = 0.2;
    vec3 vDir     = normalize( vsOut.TangentViewPos - vsOut.TangentFragPos ); // with notmal map TBN
    vec3 rDir     = reflect( -lDir, nnormals );
    vec3 hDir     = normalize( lDir + vDir );
    vec3 specular = specularStrength * lightColor * pow( max( dot( vDir, hDir ), 0.0 ), 32 );

    return vec4( ( diffuse + ambient + specular ), 1.0 );
}

void main()
{
    if( st == 0 ) { gl_FragColor = color; }
    if( st == 1 ) { gl_FragColor = color * celShader(); }
    if( st == 2 ) { gl_FragColor = diffuseShader() * mix( texture( Texture1, TexCoord ), texture( normalMap, TexCoord ), 0.1 ); }
    if( st == 3 ) 
    { 
	    if( texture( Texture1, TexCoord ).a < 0.1 ) discard;
	    gl_FragColor = color * texture( Texture1, TexCoord );
	    //gl_FragColor = vec4( 1.0, 1.0, 1.0, 1.0 );
    }
    if( st == 4 ) 
    { 
	    gl_FragColor = color * texture( Texture1, TexCoord ).r;
	    //gl_FragColor = color;
    }
}
