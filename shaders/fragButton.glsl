#version 330 core

in vec2 TexCoord;

uniform sampler2D Texture1;
uniform sampler2D Texture2;

uniform vec4 color;

void main()
{
    gl_FragColor = color;
}
