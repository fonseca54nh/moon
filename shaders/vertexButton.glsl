#version 330 core

layout(location=0) in vec4 vPosition;
layout(location=1) in vec2 vTexCoords;

out vec2 TexCoord;

uniform vec4 position;
uniform mat4 model;

void main() 
{
    gl_Position = model * vec4( vPosition + ( position ) );
    TexCoord = vTexCoords;
} 
