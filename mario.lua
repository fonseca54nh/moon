require( 'bread' )

WindowHeight = 1920
WindowWidth = 1080

Init()

MovementDimension = '2d'

local mario = Button:New( Texture( "assets/mario/marioRight1.png" ) )
mario.mesh.scale = 0.05
mario.player = true
mario.mesh:AnimationTable( "mario.anim" )

Player = mario

PlayerPosition = mario.mesh.position
PlayerOrientation = mario.Orientation

local luigi = Button:New( Texture( "assets/ocean/ocean1.png" ) )
luigi.mesh.scale = 0.04
luigi.mesh.position = { 2.1, 0.0, 0.0 }
luigi.mesh:AnimationTable( "ocean.anim" )

function MainLoop()
--	luigi.mesh:PlayAnimation( 'Idle', 3 )
--	mario:Draw( mario.mesh.position )
--	luigi:Draw( { 0, 0, 0 } )
--	luigi:Draw( { 1, 0, 0 } )
--	luigi:Draw( { 2, 0, 0 } )
--	luigi:Draw( { 3, 0, 0 } )
--	luigi:Draw( { 4, 0, 0 } )
--	luigi:Draw( { 0, 1, 0 } )
--	luigi:Draw( { 0, 2, 0 } )
--	luigi:Draw( { 0, 3, 0 } )
--	luigi:Draw( { 0, 4, 0 } )
--	luigi:Draw( { 1, 1, 0 } )
--	luigi:Draw( { 2, 2, 0 } )
--	luigi:Draw( { 3, 3, 0 } )
--	luigi:Draw( { 4, 4, 0 } )
--
--	mario:Collision2D()
	local f = loadfile( 'f.lua', 't' )
	if f then f() end
end

Keys['d'] = function() mario.mesh.position[1] = mario.mesh.position[1] + 0.1 mario.mesh:PlayAnimation( 'Right', 10 ) end
Keys['a'] = function() mario.mesh.position[1] = mario.mesh.position[1] - 0.1 mario.mesh:PlayAnimation( 'Left', 10 ) end
Keys['w'] = function() mario.mesh.position[2] = mario.mesh.position[2] + 0.1 end
Keys['s'] = function() mario.mesh.position[2] = mario.mesh.position[2] - 0.1 end

Loop( MainLoop )

