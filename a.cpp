// i'll save this here, just in case
// https://lucasklassmann.com/blog/2019-02-02-how-to-embeddeding-lua-in-c/

extern "C"{
#include <lua5.1/lualib.h>
#include <lua5.1/lauxlib.h>
#include <lua5.1/lua.h>
}

int main()
{
	lua_State *L = luaL_newstate();
	luaL_openlibs( L );

		lua_getglobal( L, "require" );
		lua_pushstring( L, "bread" );
		lua_call( L, 1, 1 );
//		lua_pushstring( L, "TestLoop()" );
//		lua_pcall( L, 0, 0, 0 );

		//execute the first part of the loop
	while( 1 )
	{
		lua_getglobal( L, "LoopInit" );
		if( lua_isfunction( L, -1 ) )
		{
			if( lua_pcall( L, 0, 1, 0 ) )
				lua_pop( L, lua_gettop( L ) );
		}

		luaL_loadfile( L, "test.lua" );
		if( lua_pcall( L, 0,0,0 ) ) //printf( "pcall error!\n" );
		{
			//execute the second part of the loop
			lua_getglobal( L, "LoopEnd" );
			if( lua_isfunction( L, -1 ) )
			{
				if( lua_pcall( L, 0, 1, 0 ) )
					lua_pop( L, lua_gettop( L ) );
			}
			lua_pop( L, lua_gettop(L) );
		}


		//if( luaL_loadstring( L, "bread.TestLoop()" ))
		//{
		//	printf( "here\n" );
		//	if( lua_pcall( L, 0,0,0 ) )
		//	{

		//	}
		//}
	}

	lua_close( L );
}
