local bread  = require( "bread"      )
local gl     = require( "moongl"     )
local glfw   = require( "moonglfw"   )
local glmath = require( "moonglmath" )

-- instance the main window
local window = Init()

-- instance the shader program
local shader = Shader( "shaders/v.glsl", "shaders/f.glsl", "3d" )
local fontShader = Shader( "shaders/vertexFont.glsl", "shaders/fragFont.glsl", "2d" )

-- use the shader
gl.use_program( shader )

-- triangle vertices
local vertices = {
     -1.0,  1.0, 0.0,
     -1.0, -1.0, 0.0,
      1.0,  1.0, 0.0,
      1.0, -1.0, 0.0,
}

local indices = {
	0, 1, 3,
	1, 2, 3,
}

-- tex coordinates
local texCoords = {
	0.0, 1.0,
	0.0, 0.0,
	1.0, 1.0,
	1.0, 0.0,
}

local triangle = Load( vertices, indices, texCoords )
local texture  = Texture( "assets/blueprint.png" )

-- Init the font
InitFont( "assets/font.ttf", 48 )

--local t, texture = LoadObj( "assets/model.obj", "assets/blueprint.png" )
local lastFrame = 0.0
local t = 0

collectgarbage( "collect" )
-- main loop
while not glfw.window_should_close( window ) do
   collectgarbage( "collect" )
   glfw.poll_events()

   t = glfw.get_time()
   Camera.dt = t - lastFrame

   gl.clear_color( 0.2, 0.2, 0.2, 0.2 )
   gl.clear( "color", "depth" )

   gl.use_program( shader )
   Draw( triangle, texture )
   --Draw( t, texture )
   gl.use_program( fontShader )
   RenderText( 'Viva Cristo Rei!', 0, -100,  1.0 )

   glfw.swap_buffers( window )
end
