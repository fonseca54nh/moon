
## Vertices

### Load( vertices )

```lua
-- load vertices
function Load( vertices )
	local vao = gl.gen_vertex_arrays()
	local vbo = gl.gen_buffers()

	gl.bind_vertex_array( vao )
	gl.bind_buffer( 'array', vbo )
	gl.buffer_data( 'array', gl.pack( 'float', vertices), 'static draw' )
	gl.vertex_attrib_pointer( 0, 3, 'float', false, 3*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array( 0 )

	gl.unbind_buffer('array')

	gl.unbind_vertex_array()

	return vao
end

```

### Load( vertices, indices, texCoords )

```lua 
-- load model with an image texture
function Load( vertices, indices, texCoords )
	-- create the buffers
	local vao  = gl.gen_vertex_arrays()
	local vbo  = gl.gen_buffers()
	local tvbo = gl.gen_buffers()

	-- bind the vertex array
	gl.bind_vertex_array( vao )

	-- vertices buffer
	gl.bind_buffer( 'array', vbo )
	gl.buffer_data( 'array', gl.pack( 'float', vertices), 'static draw' )
	gl.vertex_attrib_pointer( 0, 3, 'float', false, 3*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array( 0 )
	gl.unbind_buffer( 'array' )

	-- indices buffer
	ebo = gl.new_buffer('element array') -- element buffer object
	gl.buffer_data('element array', gl.pack('uint', indices), 'static draw')

	-- texture coordinates buffer
	gl.bind_buffer( 'array', textureVbo )
	gl.buffer_data( 'array', gl.pack( 'float', texCoords ), 'static draw' )
	gl.vertex_attrib_pointer( 1, 2, 'float', false, 2*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array(1)
	gl.unbind_buffer( 'array' )

	-- unbind the vertex array
	gl.unbind_vertex_array()

	return vao
end

```

### LoadObj

```lua  .lua
-- load 3d model with an image texture
function LoadObj( filename, a )
	--local scene, err = ai.import_file( filename, "triangulate", "join identical vertices", "sort by p type" )
	local scene, err = ai.import_file( filename, 'triangulate', 'flip uvs', 'gen smooth normals', 'calc tangent space' )
	assert( scene, err )

	local meshes = scene:meshes()

	local vertices  = {}
	local texCoords = {}
	local normals   = {}

	local hasTex = meshes[1]:has_texture_coords(1)

	for i = 1, meshes[1]:num_vertices() do
		--vertices[i] = { meshes[1]:position(i) }
		table.insert( vertices, { meshes[1]:position(i) } )
		local u, v
		if hasTex then u, v = meshes[1]:texture_coords(1, i) else u, v = 0.0, 0.0 end
		--texCoords[i] = { meshes[1]:texture_coords(1, i) }
		--texCoords[i] = { u, v }
		table.insert( texCoords, { u, v } )
		if meshes[1]:has_normals() then table.insert( normals, { meshes[1]:normal(i) } ) end
	end

	local indices = meshes[1]:all_indices( true )

	local count = #indices/gl.sizeof( 'uint' )

	return Load( vertices, indices, texCoords ), Texture( a ), count
end

```


### LoadText

```lua  .lua
-- load model with an image texture
function LoadText( vertices, indices, texCoords )
	-- bind the vertex array
	gl.bind_vertex_array( tvao )

	-- vertices buffer
	gl.bind_buffer( 'array', textVbo )
	gl.buffer_data( 'array', gl.pack( 'float', vertices), 'static draw' )
	gl.vertex_attrib_pointer( 0, 3, 'float', false, 3*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array( 0 )
	gl.unbind_buffer( 'array' )

	-- indices buffer
	ebo = gl.new_buffer('element array') -- element buffer object
	gl.buffer_data('element array', gl.pack('uint', indices), 'static draw')

	-- texture coordinates buffer
	gl.bind_buffer( 'array', textTextureVbo )
	gl.buffer_data( 'array', gl.pack( 'float', texCoords ), 'static draw' )
	gl.vertex_attrib_pointer( 1, 2, 'float', false, 2*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array(1)
	gl.unbind_buffer( 'array' )

	-- unbind the vertex array
	gl.unbind_vertex_array()

	return vao
end

```

## Draw

```lua
-- render vertices
function Draw( vao )
	gl.bind_vertex_array( vao )
	gl.draw_arrays( 'triangles', 0, 3 )
	gl.unbind_vertex_array()
end

-- render vertices with a texture
function Draw( vao, texture, count )
	gl.enable( 'depth test' )
	gl.depth_func( 'less' )

	ComputeMVP()
	gl.bind_vertex_array( vao )
	gl.bind_texture( '2d', texture )
	--gl.draw_arrays( 'triangle strip', 0, 1600 )
	gl.draw_elements( 'triangle strip', 1600*count, 'uint', 0 )
	gl.unbind_vertex_array()
	gl.disable( "depth test" )
end

-- render text vertices
function DrawText( vao, texture )
	gl.bind_vertex_array( tvao )
	gl.bind_texture( '2d', texture )
	gl.draw_arrays( 'triangles', 0, 18 )
	gl.unbind_vertex_array()
end
```

### ProcessInput

```lua
function ProcessInput()
	if MovementDimension == '2d' then
	local chars = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' }

	for _,i in ipairs( chars ) do
		if glfw.get_key( window, i ) == 'press' then Keys[ i ]() end
	end
--		Player.mesh:PlayAnimation( 'Idle', 10 )
--		if glfw.get_key( window, 'w' ) == 'press' then
--			PreviousButton = 'w'
--			PlayerPosition[2] = PlayerPosition[2] + 0.1
--		end
--
--		if glfw.get_key( window, 's' ) == 'press' then
--			PreviousButton = 's'
--			PlayerPosition[2] = PlayerPosition[2] - 0.1
--		end
--
--		if glfw.get_key( window, 'a' ) == 'press' then
--			PreviousButton = 'a'
--			PlayerPosition[1] = PlayerPosition[1] - 0.1
--			PlayerOrientation = -1.0
--			Player.mesh:PlayAnimation( 'Right', 10 )
--		end
--
--		if glfw.get_key( window, 'd' ) == 'press' then
--			PreviousButton = 'd'
--			PlayerPosition[1] = PlayerPosition[1] + 0.1
--			PlayerOrientation = 1.0
--			Player.mesh:PlayAnimation( 'Right', 10 )
--		end
--
--		if glfw.get_key( window, 'q' ) == 'press' then
--			glfw.set_window_should_close( true )
--		end
--
--		if glfw.get_key( window, 'b' ) == 'press' then
--			Change = 1
--		else
--			Change = 0
--		end

	else
		if glfw.get_key( window, 'w' ) == 'press' then
			--Camera.position.y = Camera.position.y - 1 * Camera.sensitivity
			Camera.position = Camera.position + Camera.up * Camera.sensitivity 
		end

		if glfw.get_key( window, 's' ) == 'press' then
			--Camera.position.y = Camera.position.y + 1 * Camera.sensitivity
			Camera.position = Camera.position - Camera.up * Camera.sensitivity
		end

		if glfw.get_key( window, 'q' ) == 'press' then
			glfw.set_window_should_close( true )
		end

		if glfw.get_key( window, 'up' ) == 'press' then
			Camera.position = Camera.position + Camera.front*Camera.speed * Camera.dt
		end

		if glfw.get_key( window, 'down' ) == 'press' then
			Camera.position = Camera.position - Camera.front*Camera.speed*Camera.dt
		end

		if glfw.get_key( window, 'left' ) == 'press' then
			--Camera.position.x = Camera.position.x - 1
			Camera.position = Camera.position - Camera.right * Camera.speed*Camera.dt
		end

		if glfw.get_key( window, 'right' ) == 'press' then
			Camera.position.x = Camera.position.x + 1
		end

		if glfw.get_key( window, 'd' ) == 'press' then
			--Camera.front = glmath.mat3( glmath.rotate_y( rad( -( glfw.get_mouse ) ) ) ) * Camera.front 
			Camera.front = glmath.mat3( glmath.rotate_y( rad( -( 1.0 ) ) ) ) * Camera.front 
			Camera.right       = (Camera.front % Camera.up):normalize()
		end

		if glfw.get_key( window, 'a' ) == 'press' then
			Camera.front = glmath.mat3( glmath.rotate_y( rad( 1.0 ) ) ) * Camera.front 
			Camera.right       = (Camera.front % Camera.up):normalize()
		end
	end
end

```

## Font

### Definition

```lua
Text = {}
Text.__index = Text
Text.mesh     = nil
Text.fontFace = nil

```

### InitFont

```lua
-- init the default font
function InitFont( filename, size )
	local lib = ft.init_freetype()

	local face = ft.new_face( lib, filename )
	face:set_pixel_sizes( 0, size )
	gl.pixel_store('unpack alignment', 1)

	face:select_charmap( 1 )

	face:load_char( 64, 0 )

	--local glyph = face:glyph()

	for c in string.gmatch( chars, "." ) do
		face:load_char( c, ft.LOAD_RENDER )
		local glyph = face:glyph()
		table.insert( textureTable, { c, glyph, glyph.bitmap, TextTexture( glyph ) } )
	end

	lib:done()
	--return glyph
end

```

### TextTexture

```lua
-- Create a texture fot the selected font
function TextTexture( glyph )
	local texture = gl.gen_textures()
	gl.bind_texture( '2d', texture )

	gl.texture_parameter( '2d', 'wrap s', 'clamp to edge' )
	gl.texture_parameter( '2d', 'wrap t', 'clamp to edge' )
	gl.texture_parameter( '2d', 'min filter', 'linear' )
	gl.texture_parameter( '2d', 'mag filter', 'linear' )

	gl.texture_image( '2d', 0, 'red', 'red', 'ubyte', glyph.bitmap.buffer, glyph.bitmap.width, glyph.bitmap.rows )
	gl.generate_mipmap( '2d' )

	return texture
end

```

### getSize

```lua
function getSize( str, fscale )
	fscale = fscale/10

	local sizex = 0
	local sizey = 0

	for char in string.gmatch( str, '.' ) do
		for index, item in ipairs( textureTable ) do
			if char == item[1] then
				local w = item[3].width * fscale
				local h = item[3].rows  * fscale
				sizex = sizex + w
				if  h > sizey then sizey = h end
				end
			end
		end
	return sizex, sizey
end

```

### RenderText

```lua

-- render a string at x and y with scale
function RenderText( str, x, y, fscale )
	fscale = fscale/10
	gl.enable( "cull face" )
	gl.enable( "blend" )
	gl.blend_func( "src alpha", "one minus src alpha" )

	local totalSize = 0
	-- move all this to the InitFont function
	local lastLetterAdvance = 0
	for char in string.gmatch( str, '.' ) do
		for index, item in ipairs( textureTable ) do
			if char == item[1] then
				local xpos = x + item[3].left * fscale
				local ypos = y - (item[3].rows - item[3].top) * fscale

				local w = item[3].width * fscale
				local h = item[3].rows  * fscale

				totalSize = totalSize + w

				local vertices =
				{
					( xpos     ) /100 , ( ypos + h ) /100, 0.0,
					( xpos     ) /100 , ( ypos     ) /100, 0.0,
					( xpos + w ) /100 , ( ypos     ) /100, 0.0,

					( xpos     ) /100 , ( ypos + h ) /100, 0.0,
					( xpos + w ) /100 , ( ypos     ) /100, 0.0,
					( xpos + w ) /100 , ( ypos + h ) /100, 0.0,
				};

				local texCoords =
				{
					0.0, 0.0,
					0.0, 1.0,
					1.0, 1.0,

					0.0, 0.0,
					1.0, 1.0,
					1.0, 0.0
				};

				local indices = {
					0, 1, 3,
					1, 2, 3,
				}

				local letter = LoadText( vertices, indices, texCoords )
				local letterTexture = item[4]
				DrawText( letter, letterTexture )
				x = x + math.floor( item[2].advance.x / 2 ^ 6 ) * fscale
			end
		end
	end
	gl.disable( 'blend' )
	Console( "Total Text Size: " .. totalSize )
end

function DrawText( vao, texture )
	gl.bind_vertex_array( tvao )
	gl.bind_texture( '2d', texture )
	gl.draw_arrays( 'triangles', 0, 18 )
	gl.unbind_vertex_array()
end

function LoadText( vertices, indices, texCoords )
	-- bind the vertex array
	gl.bind_vertex_array( tvao )

	-- vertices buffer
	gl.bind_buffer( 'array', textVbo )
	gl.buffer_data( 'array', gl.pack( 'float', vertices), 'static draw' )
	gl.vertex_attrib_pointer( 0, 3, 'float', false, 3*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array( 0 )
	gl.unbind_buffer( 'array' )

	-- indices buffer
	ebo = gl.new_buffer('element array') -- element buffer object
	gl.buffer_data('element array', gl.pack('uint', indices), 'static draw')

	-- texture coordinates buffer
	gl.bind_buffer( 'array', textTextureVbo )
	gl.buffer_data( 'array', gl.pack( 'float', texCoords ), 'static draw' )
	gl.vertex_attrib_pointer( 1, 2, 'float', false, 2*gl.sizeof( 'float' ), 0 )
	gl.enable_vertex_attrib_array(1)
	gl.unbind_buffer( 'array' )

	-- unbind the vertex array
	gl.unbind_vertex_array()

	return vao
end

```

