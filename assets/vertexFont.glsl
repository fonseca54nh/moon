#version 330 core

layout(location=0) in vec4 vPosition;
layout(location=1) in vec2 vTexCoords;

out vec2 TexCoord;

void main() 
{
    gl_Position = vec4( vPosition );
    TexCoord = vTexCoords;
} 
